## ESP Powered Chicken Coop Controller
This is a project to control two doors on a chicken coop. Using an esp32 allowing connection to a thingsboard page via 

MQTT and also allowing local (on the enclosure) controls. 


The project communicates over local WIFI to a localy hosted instance of thingsboard via the MQTT protocol. This allows
overriding control of the coop doors and monitoring of door state, tempurature and some diagnostics. 



# State Event Design Pattern
This project implements the State Event Design Patter described by the following paper 
https://dl.acm.org/doi/pdf/10.1145/2721956.2721987. This allows the state machine to be triggered by
multiple inputs without the source becoming an unmanageable endlesss chain of if-else statements. 

# Development Env
Currently the project requires PlatformIO to install and manage dependencies. 

`[insert intialization command]` 

# Hardware
Currently, the hardware is a 2nd generation PCB designed around the ESP32-WROOM-32U module. The gerber files for the 
PCB may be provided eventually. But all the Pins are defined as constants and should be able to be adapted to any 
arduino capable board. 


# Testing
I have been unable to get any form of meaningful unit tests working. As the foundation of this app is the Arduinio
framework, and I have been unable to mock it or substitute.  At the current, the only path to testing I can see is to
run tests deploying to actual hardware. This is less than ideal, and would make CI testing setup difficult and fragile.
Attempting to test/ mock this project has helped me to appreciate the IOC pattern, such as found in the Laravel 
PHP framework. 