//
// Created by andrew on 10/10/20.
//

#include "Context.h"
#include "State/DoorClosed.h"
#include "State/DoorOpen.h"

/**
 * Set the starting positions
 * @param doorOneStart
 * @param doorTwoStart
 */
Context::Context():
        doorOneOpenState( "doorOneOpen"),
        doorOneOpeningState("doorOneOpening", DOOR_ONE_OPEN_OUT, DOOR_ONE_CLOSE_OUT),
        doorTwoOpenState("doorTwoOpen"),
        doorTwoOpeningState("doorTwoOpening", DOOR_TWO_OPEN_OUT, DOOR_TWO_CLOSE_OUT),
        doorOneCloseState("doorOneClose"),
        doorOneClosingState("doorOneClosing", DOOR_ONE_CLOSE_OUT, DOOR_ONE_OPEN_OUT),
        doorTwoCloseState( "doorTwoClose"),
        doorTwoClosingState("doorTwoClosing", DOOR_TWO_CLOSE_OUT, DOOR_TWO_OPEN_OUT),
        doorOneCloseEvent(),
        doorOneClosedEvent(),
        doorTwoCloseEvent(),
        doorTwoClosedEvent(),
        doorOneOpenEvent(),
        doorOneOpenedEvent(),
        doorTwoOpenEvent(),
        doorTwoOpenedEvent(){
    this->current_door_one_state = &this->doorOneOpeningState;
    this->current_door_two_state = &this->doorTwoOpeningState;
    this->setTransitions();
}

/**
 * Fire event for door one
 * @param event
 */
void Context::doorOneDispatch(Event* event) {
    this->current_door_one_state = event->getTargetState(this->current_door_one_state);
}

/**
 * Fire event for door two
 * @param event
 */
void Context::doorTwoDispatch(Event* event) {
    this->current_door_two_state = event->getTargetState(this->current_door_two_state);
}

/**
 * Set transitions for each event
 */
void Context::setTransitions() {
    // Door One Open Event Transitions
    this->doorOneOpenEvent.addTransition(this->doorOneCloseState, &this->doorOneOpeningState);
    this->doorOneOpenEvent.addTransition(this->doorOneClosingState, &this->doorOneOpeningState);
    this->doorOneOpenEvent.addTransition(this->doorOneOpeningState, &this->doorOneOpeningState);
    this->doorOneOpenEvent.addTransition(this->doorOneOpenState, &this->doorOneOpenState);

    // Door One Opened Event Transitions
    this->doorOneOpenedEvent.addTransition(this->doorOneOpeningState, &this->doorOneOpenState);

    // Door One Close Event Transitions
    this->doorOneCloseEvent.addTransition(this->doorOneOpenState, &this->doorOneClosingState);
    this->doorOneCloseEvent.addTransition(this->doorOneOpeningState, &this->doorOneClosingState);
    this->doorOneCloseEvent.addTransition(this->doorOneClosingState, &this->doorOneClosingState);
    this->doorOneCloseEvent.addTransition(this->doorOneCloseState, &this->doorOneCloseState);

    // Door One Closed Event Transitions
    this->doorOneClosedEvent.addTransition(this->doorOneClosingState, &this->doorOneCloseState);

    // Door Two Open Event Transitions
    this->doorTwoOpenEvent.addTransition(this->doorTwoCloseState, &this->doorTwoOpeningState);
    this->doorTwoOpenEvent.addTransition(this->doorTwoClosingState, &this->doorTwoOpeningState);
    this->doorTwoOpenEvent.addTransition(this->doorTwoOpeningState, &this->doorTwoOpeningState);
    this->doorTwoOpenEvent.addTransition(this->doorTwoOpenState, &this->doorTwoOpenState);

    // Door Two Opened Event Transitions
    this->doorTwoOpenedEvent.addTransition(this->doorTwoOpeningState, &this->doorTwoOpenState);

    // Door Two Close Event Transitions
    this->doorTwoCloseEvent.addTransition(this->doorTwoOpenState, &this->doorTwoClosingState);
    this->doorTwoCloseEvent.addTransition(this->doorTwoOpeningState, &this->doorTwoClosingState);
    this->doorTwoCloseEvent.addTransition(this->doorTwoClosingState, &this->doorTwoClosingState);
    this->doorTwoCloseEvent.addTransition(this->doorTwoCloseState, &this->doorTwoCloseState);

    // Door Two Closed Event Transition
    this->doorTwoClosedEvent.addTransition(this->doorTwoClosingState, &this->doorTwoCloseState);
}

/**
 * Get string 'state' of door one
 * @return
 */
std::string Context::getDoorOneStateStr(){
    return this->current_door_one_state->getName();
}

/**
 * Get string 'state' of door two
 * @return
 */
std::string  Context::getDoorTwoStateStr(){
    return this->current_door_two_state->getName();
}

/**
 * Get door one state enum
 * @return
 */
doorState Context::getDoorOneState(){
     return this->current_door_one_state->type();
}

/**
 * Get door two state enum
 * @return
 */
doorState Context::getDoorTwoState(){
   return this->current_door_two_state->type();
}