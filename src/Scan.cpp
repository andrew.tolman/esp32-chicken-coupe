//
// Created by andrew on 10/10/20.
//

#include <State/DoorOpen.h>
#include "Scan.h"
#include "Event/CloseEvent.h"
#include "Event/OpenEvent.h"
#include <Arduino.h>

Scan::Scan():
        statusLed(STATUS_LED_OUT),
        errorLed(ERROR_LED_OUT),
        DoorOneOpenSwtch(DOOR_ONE_OPEN_IN),
        DoorOneCloseSwtch(DOOR_ONE_CLOSE_IN),
        DoorTwoOpenSwtch(DOOR_TWO_OPEN_IN),
        DoorTwoCloseSwtch(DOOR_TWO_CLOSE_IN),
        FSM(),
        TimeTracker(TZ_MINUTES, LATITUDE, LONGITUDE),
        doorOneRemoteOverride(Nill),
        doorOneLocalOverride(Nill),
        doorTwoRemoteOverride(Nill),
        doorTwoLocalOverride(Nill),
        sensorOne(COOP_TEMP_SENSOR),
        doorOneMotionTargetMillis(10),
        doorTwoMotionTargetMillis(10){
    errorLed.setPinState(false);
    statusLed.setPinState(true);
    this->DoorOneEvent = nullptr;
    this->DoorTwoEvent = nullptr;
}

/**
 * Sequence to read remote inputs
 *  1. If we are not in local override then
 *      2. check command for setting door one
 *         if state command true then
 *              open
 *         if state command false then
 *              close
 *      3. Check for override reset call
 * @param method
 * @param stateCommand
 */
void Scan::readRemoteInput(const char* method, bool stateCommand) {
    std::string command = std::string(method);

    // Only if we are not in local over ride
    if (this->doorOneLocalOverride == Nill) {
        // if the rpc is for door one
        if (command == "setDoorOneCommandState") {
            if (stateCommand) {
                if (this->doorOneRemoteOverride != Open || this->doorOneRemoteOverride != Opening) {
                    this->DoorOneEvent = &this->FSM.doorOneOpenEvent;
                    this->doorOneRemoteOverride = Opening;
                    debug_print("Read Remote Input: door one open command");
                }
            } else {
                if (this->doorOneRemoteOverride != Closed || this->doorOneRemoteOverride != Closing) {
                    this->doorOneRemoteOverride = Closing;
                    this->DoorOneEvent = &this->FSM.doorOneCloseEvent;
                    debug_print("Read Remote Input: door one close command");
                }
            }
        }

        // TODO Optimize, check for leaks
        if (command == "resetDoorOneCommand") {
            this->doorOneRemoteOverride = Nill;
            debug_print("Read Remote Input: door one override release command");
        }
    }

    // Only if we are not in local over ride
    if (this->doorTwoLocalOverride == Nill) {

        // if the rpc is for door two
        if (command == "setDoorTwoCommandState") {
            if (stateCommand) {
                if (this->doorTwoRemoteOverride != Open || this->doorTwoRemoteOverride != Opening) {
                    this->DoorTwoEvent = &this->FSM.doorTwoOpenEvent;
                    this->doorTwoRemoteOverride = Opening;
                    debug_print("Read Remote Input: door two open command");
                }
            } else {
                if (this->doorTwoRemoteOverride != Closed || this->doorTwoRemoteOverride != Closing) {
                    this->DoorTwoEvent = &this->FSM.doorTwoCloseEvent;
                    this->doorTwoRemoteOverride = Closing;
                    debug_print("Read Remote Input: door two close command");
                }
            }
        }

        if (command == "resetDoorTwoCommand") {
            this->doorTwoRemoteOverride = Nill;
            debug_print("Read Remote Input: door two override release command");
        }
    }
}

/**
 * Sequence that reads from physical inputs
 */
void Scan::readPhyInput() {

    /**
     * Door One Open check:
     *  1. if switch is on then set override and set open event
     *  2. if switch is off and we were previously in override then reset
     */
    if (this->DoorOneOpenSwtch.readPinState()) {
        // Physical pin is toggled, and door is not in position, then fire event & set override
        if (this->doorOneLocalOverride != Open && this->doorOneLocalOverride != Opening) {
            this->doorOneLocalOverride = Opening;
            this->DoorOneEvent = &this->FSM.doorOneOpenEvent;
            debug_print("Read Physical Input: door one open switch open");
        }

        // if pin is false && it was previously in override then reset
    } else if(this->doorOneLocalOverride == Open || this->doorOneLocalOverride == Opening) {
        this->doorOneLocalOverride = Nill;
        this->DoorOneEvent =  &this->FSM.doorOneCloseEvent;
        debug_print("Read Physical Input: door one open switch closed");
    }

    /**
     * Door One Close check:
     *  1. if switch is on then set override and set open event
     *      1.a if event is already set, we have multiple set soft panic
     *  2. if switch is off and we were previously in override then reset
     */
    if (this->DoorOneCloseSwtch.readPinState()) {
        // Physical pin is toggled, and door is not in position, then fire event & set override
        if (this->doorOneLocalOverride != Closed && this->doorOneLocalOverride != Closing) {
            if (this->DoorOneEvent != nullptr) {
                this->errorLed.setPinState(true);
                Serial.print(F("Error multiple input commands received door one, defaulting to door open"));

            } else {
                this->doorOneLocalOverride = Closing;
                this->DoorOneEvent = &this->FSM.doorOneCloseEvent;
                debug_print("Read Physical Input: door one close switch open");
            }
        }

        // if pin is false && it was previously in override then reset
    } else if(this->doorOneLocalOverride == Closed || this->doorOneLocalOverride == Closing) {
        this->doorOneLocalOverride = Nill;
        this->DoorOneEvent =  &this->FSM.doorOneOpenEvent;
    }

    /**
      * Door One Open check:
      *  1. if switch is on then set override and set open event
      *  2. if switch is off and we were previously in override then reset
      */
    if (this->DoorTwoOpenSwtch.readPinState()) {
        // Physical pin is toggled, and door is not in position, then fire event & set override
        if (this->doorTwoLocalOverride != Open && this->doorTwoLocalOverride != Opening) {
            this->doorTwoLocalOverride = Open;
            this->DoorTwoEvent = &this->FSM.doorTwoOpenEvent;
            debug_print("Read Physical Input: door two open switch open");
        }

        // if pin is false && it was previously in override then reset
    } else if(this->doorTwoLocalOverride == Open || this->doorTwoLocalOverride == Opening) {
        this->doorTwoLocalOverride = Nill;
        this->DoorTwoEvent =  &this->FSM.doorTwoCloseEvent;
        debug_print("Read Physical Input: door two open switch closed");
    }

    /**
     * Door Two Close check:
     *  1. if switch is on then set override and set open event
     *      1.a if event is already set, we have multiple set soft panic
     *  2. if switch is off and we were previously in override then reset
     */
    if (this->DoorTwoCloseSwtch.readPinState()) {
        // Physical pin is toggled, and door is not in position, then fire event & set override
        if (this->doorTwoLocalOverride != Closed && this->doorTwoLocalOverride != Closing) {
            if (this->DoorTwoEvent != nullptr) {
                this->errorLed.setPinState(true);
                Serial.print(F("Error multiple input commands received door two, defaulting to door open"));

            } else {
                this->doorTwoLocalOverride = Closing;
                this->DoorTwoEvent = &this->FSM.doorTwoCloseEvent;
                debug_print("Read Physical Input: door two close switch open");
            }
        }

        // if pin is false && it was previously in override then reset
    } else if(this->doorTwoLocalOverride == Closed || this->doorTwoLocalOverride == Closing) {
        this->doorTwoLocalOverride = Nill;
        this->DoorTwoEvent =  &this->FSM.doorTwoOpenEvent;
        debug_print("Read Physical Input: door two close switch close");
    }
}

/**
 * Read temperature
 */
void Scan::readTemp() {
    this->sensorOne.sendTempRequest();
    debug_print("Read Temp: Temp Requested");
    this->sensorOneTemp = this->sensorOne.getFTemp();
    char temp[20];
    sprintf(temp, "%f", this->sensorOneTemp);
    debug_print("Read Temp: Temp grabbed");
    debug_print(temp);
    if (this->sensorOneTemp == DEVICE_DISCONNECTED_F) {
        this->error += "ReadTemp: failed to get valid temp; ";
        debug_print("Read Temp: failed to get valid temp;");
    }
}


/**
 * Check the time (sun time) to see if what its input is (door open/close)
 */
 void Scan::checkSun() {
     // check if the door is supposed to be open or closed;
    doorState DoorPosition = this->TimeTracker.checkTime();

    // Only if there are no pending events && we are not in over ride
    if (this->DoorOneEvent == nullptr
        && this->doorOneLocalOverride == Nill
        && this->doorOneRemoteOverride == Nill) {
        if (DoorPosition != FSM.getDoorOneState()) {
            // since we are not in the desired position we might need to dispatch an event
            if (DoorPosition == Open) {
                // Only if the door is not currently opening
                if (FSM.getDoorOneState() != Opening) {
                    this->DoorOneEvent = &this->FSM.doorOneOpenEvent;
                    debug_print("Time Event: door one open event log");
                }
            } else {
                // If we are not currently closing the door throw an event on the pile
                if (FSM.getDoorOneState() != Closing) {
                    this->DoorOneEvent = &this->FSM.doorOneCloseEvent;
                    debug_print("Time Event: door one close event log");
                }
            }
        }
    }

    // Only if there are no pending events && we are not in override
    if (this->DoorTwoEvent == nullptr
        && this->doorTwoLocalOverride == Nill
        && this->doorTwoRemoteOverride == Nill) {
        if (DoorPosition != FSM.getDoorTwoState()) {
            // We will only ever auto close the door!
            if (DoorPosition != Open) {
                // If we are not currently opening the door then throw an event
                if (FSM.getDoorTwoState() != Closing) {
                    this->DoorTwoEvent = &this->FSM.doorTwoCloseEvent;
                    debug_print("Time Event: door two close event log");
                }
            }
        }
    }
 }

/**
 * Write digital outputs
 *  TODO: set callback & cancel any outstanding callback
 */
void Scan::writeOutput() {
    if (this->DoorOneEvent != nullptr) {
        // Check for an open or close event, if present then start timer for the state
        if (this->DoorOneEvent == &this->FSM.doorOneOpenEvent
           || this->DoorOneEvent == &this->FSM.doorOneCloseEvent) {
            debug_print("Write Physical Output: door one delay millis set");
            this->doorOneMotionTargetMillis = millis() + (ACTUATION_S * MILLIS_TO_S);
        }
        this->FSM.doorOneDispatch(this->DoorOneEvent);
        debug_print("Write Physical Output: door one Event fired");
        this->DoorOneEvent = nullptr;
    }

    if (this->DoorTwoEvent != nullptr) {
        // Check for an open or close event, if present then start timer for the state
        if (this->DoorTwoEvent == &this->FSM.doorTwoOpenEvent
        || this->DoorTwoEvent == &this->FSM.doorTwoCloseEvent) {
            debug_print("Write Physical Output: door two delay millis set");
            this->doorTwoMotionTargetMillis = millis() + (ACTUATION_S * MILLIS_TO_S);
        }
        this->FSM.doorTwoDispatch(this->DoorTwoEvent);
        debug_print("Write Physical Output: door two Event fired");
        this->DoorTwoEvent = nullptr;
    }
}

/**
 * Delayed task check
 */
void Scan::checkDelayed() {
    if (this->doorOneMotionTargetMillis != 0
     && this->doorOneMotionTargetMillis <= millis()) {
        debug_print("Delay Check: door one target millis reached reset fired");
        this->doorOneOutputReset();
    }

    if (this->doorTwoMotionTargetMillis != 0
    && this->doorTwoMotionTargetMillis <= millis()) {
        debug_print("Delay Check: door two target millis reached reset fired");
        this->doorTwoOutputReset();
    }
}

/**
 * Fire the target position event and reset counter
 */
void Scan::doorOneOutputReset() {
    if (this->FSM.getDoorOneState() == Opening){
        this->DoorOneEvent = &FSM.doorOneOpenedEvent;
        debug_print("Delay One Delay Reset: door one opened fired");
    } else if (this->FSM.getDoorOneState() == Closing) {
        this->DoorOneEvent = &FSM.doorOneClosedEvent;
        debug_print("Delay One Delay Reset: door one closed fired");
    } else {
        debug_print("Delay One Delay Reset: Error reset on non moving state");
    }
    this->doorOneMotionTargetMillis = 0;
}

/**
 *  Fire the target position event and reset counter
 */
void Scan::doorTwoOutputReset() {
    if (this->FSM.getDoorTwoState() == Opening) {
        this->DoorTwoEvent = &FSM.doorTwoOpenedEvent;
        debug_print("Delay Two Delay Reset: door two opened fired");
    } else if (this->FSM.getDoorTwoState() == Closing) {
        this->DoorTwoEvent = &FSM.doorTwoClosedEvent;
        debug_print("Delay Two Delay Reset: door two closed fired");
    } else {
        debug_print("Delay Two Delay Reset: Error reset on non moving state");
    }
    // reset the counter
    this->doorTwoMotionTargetMillis = 0;
}


/**
 * Send current status (door one/two for now)
 * @param client
 */
void Scan::sendStatus(EspMQTTClient& client) {

    try {

        // Temp info
        char temp[20];
        sprintf(temp, "%1.0f", this->sensorOneTemp);

        std::string jsonObj = R"({"temperature": )";
        jsonObj += temp;
        jsonObj += "}";

        client.publish("v1/devices/me/telemetry", jsonObj.c_str());

        // Door one info
        jsonObj = R"({"doorOne": ")" + this->FSM.getDoorOneStateStr() + "\",";
        jsonObj += R"("doorOneLocalOverride":")" + this->getOverrideStatusStrings(this->doorOneLocalOverride) + "\",";
        jsonObj += R"("doorOneRemoteOverride":")" + this->getOverrideStatusStrings(this->doorOneRemoteOverride) + "\"}";

       client.publish("v1/devices/me/telemetry", jsonObj.c_str());


       // Door Two info
        jsonObj = R"({"doorTwo": ")" + this->FSM.getDoorTwoStateStr() + "\",";
        jsonObj += R"("doorTwoLocalOverride":")" + this->getOverrideStatusStrings(this->doorTwoLocalOverride) + "\",";
        jsonObj += R"("doorTwoRemoteOverride":")" + this->getOverrideStatusStrings(this->doorTwoRemoteOverride) + "\"}";

        client.publish("v1/devices/me/telemetry", jsonObj.c_str());

        // Time info
        jsonObj = R"({"currentTime": ")";
        this->TimeTracker.getCurrentTimeStr(jsonObj);
        jsonObj+= "\",";

        jsonObj += R"("sunriseSeconds":")";
        this->TimeTracker.getSunriseStr(jsonObj);
        jsonObj += "\",";
        jsonObj += R"("sunsetSeconds":")";
        this->TimeTracker.getSunsetStr(jsonObj);
        jsonObj += "\"}";

        client.publish("v1/devices/me/telemetry", jsonObj.c_str());

    } catch (...) {
        Serial.print(F("Exception trying to publish"));
    }
}

/**
 * TODO Implement error state
 * @param error
 */
void Scan::setError(std::string& error) {
    this->errorLed.setPinState(true);
    this->error = error;
}

/**
 * Reach out and set system time via ntp server
 */
void Scan::setTime() {
    configTime(this->gmtOffset_sec, this->daylightOffset_sec, this->ntpServer);
    struct tm timeinfo{};
    if(!getLocalTime(&timeinfo)){
        Serial.println("Failed to obtain time");
        return;
    }
    Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
}


/**
 * Grab the string from the override enum
 * @param state
 * @return
 */
std::string Scan::getOverrideStatusStrings(doorState state){
    std::string returnString;
    switch (state) {
        case Open:
            returnString = "Open";
            break;
        case Opening:
            returnString = "Opening";
            break;
        case Closed:
            returnString = "Closed";
            break;
        case Closing:
            returnString = "Closing";
            break;
        case Nill:
            returnString = "Nill";
            break;
    };
    return returnString;
}