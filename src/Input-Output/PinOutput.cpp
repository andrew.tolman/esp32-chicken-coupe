//
// Created by andrew on 2/23/20.
//


#include "PinOutput.h"

PinOutput::PinOutput(const int boardGpioNumber): boardGPIONumber(boardGpioNumber)
{
    pinMode(boardGpioNumber, OUTPUT);
    digitalWrite(boardGpioNumber, true);
    this->setPinState(true);
}

/**
 * Setter
 * @param value
 */
void PinOutput::setPinState(const bool value) {
    if (value != this->pinState) {
        digitalWrite(this->boardGPIONumber, value);
        this->pinState = value;
    }
}

/**
 * Setting the board GPIO number
 * @param value
 */
void PinOutput::setBoardGPIONumber(const int value) {
    this->boardGPIONumber = value;
}