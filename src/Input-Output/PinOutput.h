//
// Created by andrew on 2/23/20.
//

#ifndef ESP_MQTT_PINOUTPUT_H
#define ESP_MQTT_PINOUTPUT_H
#include <Arduino.h>
#include <debug.h>
// Define DEBUG to enable debug print statements
#ifndef DEBUG
#define DEBUG 0
#endif


class PinOutput {
private:
    bool pinState;
    int boardGPIONumber;
public:
    PinOutput(int boardGpioNumber);
    void setPinState(bool value);
    void setBoardGPIONumber(int value);
};


#endif //ESP_MQTT_PINOUTPUT_H
