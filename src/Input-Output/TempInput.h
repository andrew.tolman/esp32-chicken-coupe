//
// Created by andrew on 11/30/20.
//

#ifndef ESP_COOP_TEMPINPUT_H
#define ESP_COOP_TEMPINPUT_H

#include <OneWire.h>
#include <DallasTemperature.h>

class TempInput {
private:
    OneWire oneWire;
    DallasTemperature sensor;
public:
    TempInput(int pin);
    void sendTempRequest();
    float getCTemp();
    float getFTemp();
};


#endif //ESP_COOP_TEMPINPUT_H
