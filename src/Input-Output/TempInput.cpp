//
// Created by andrew on 11/30/20.
//

#include "TempInput.h"

TempInput::TempInput(int pin): oneWire(pin), sensor(&oneWire){
    this->sensor.begin();
}

/**
 * Send the temp request to the device
 */
void TempInput::sendTempRequest() {
    this->sensor.requestTemperatures();
}

/**
 *  Get temperature in C
 * @return
 */
float TempInput::getCTemp() {
   return this->sensor.getTempCByIndex(0);
}

/**
 * Get temperature in F
 * @return
 */
float TempInput::getFTemp() {
   return this->sensor.getTempFByIndex(0);
}