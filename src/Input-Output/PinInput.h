//
// Created by andrew on 10/10/20.
//

#ifndef ESP_MQTT_PININPUT_H
#define ESP_MQTT_PININPUT_H
#include <Arduino.h>


class PinInput {
private:
    bool pinState;
    int boardGPIONumber;
public:
    PinInput(int boardGpioNumber);
    void setBoardGPIONumber(const int value);
    bool readPinState();
};


#endif //ESP_MQTT_PININPUT_H
