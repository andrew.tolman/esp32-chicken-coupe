//
// Created by andrew on 10/10/20.
//

#include "PinInput.h"


PinInput::PinInput(int boardGpioNumber) {
    pinMode(boardGpioNumber, INPUT_PULLDOWN);
    this->pinState = false;
    this->setBoardGPIONumber(boardGpioNumber);
}

void PinInput::setBoardGPIONumber(const int value) {
    this->boardGPIONumber = value;
}

bool PinInput::readPinState() {
    this->pinState = digitalRead(this->boardGPIONumber);
    return this->pinState;

}