//
// Created by andrew on 2/17/20.
//

#include <Arduino.h>

#include <EspMQTTClient.h>
#include <ArduinoJson.h>
#include "Scan.h"
#include "debug.h"

// Define DEBUG to enable debug print statements
#ifndef DEBUG
#define DEBUG 0
#endif

#define uS_TO_S_FACTOR 1000000  /* Conversion factor for micro seconds to seconds */
#define TIME_TO_SLEEP  5        /* Time ESP32 will go to sleep (in seconds) */

EspMQTTClient client(
        "Newt",
        "Fighto4Life",
        "debian-node-1.lan", // MQTT Broker server ip
        "woq7RyfmmrB7N95fB7gk", // Can be omitted if not needed
        __null,
        "ESP32"
        );
// Allocate the JSON document
//
// Inside the brackets, 200 is the capacity of the memory pool in bytes.
// Don't forget to change this value to match your JSON document.
// Use arduinojson.org/v6/assistant to compute the capacity.
StaticJsonDocument<200> doc;

Scan* Routine;
bool isConnected = false;


void setup() {
    Serial.begin(9600);
    //client.enableDebuggingMessages(); // Enable debugging messages sent to serial output
    Routine = new Scan();
    client.setMaxPacketSize(256);
}
 /**
  *          {"method":"setValue","params":true}
  */
void onConnectionEstablished() {

    client.subscribe("v1/devices/me/rpc/request/+", [] (const String &topic, const String &payload)  {
        Serial.println(payload);



        // Deserialize the JSON document
        DeserializationError error = deserializeJson(doc, payload);

        // Test if parsing succeeds.
        if (error) {
            Serial.print(F("deserializeJson() failed: "));
            Serial.println(error.c_str());
            return;
        }

        const char* method = doc["method"];
        const bool pin = doc["params"];
        Serial.println(method);
        Serial.println(pin ? "true" : "false");
        Routine->readRemoteInput(method, pin);
    });
    isConnected = true;
    // reach out to the ntp and grab time
    Routine->setTime();
     // Setup sleep timer
     //esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
}

void loop() {

    // We have to call loop at least 3 time if the wifi status changes
    for (int loopIndex = 0; loopIndex < 3; loopIndex++){
        client.loop();
        // if we are connected then no need to continue looping
        if (client.isConnected()) {
            break;
        }
    }

    // Read Input switches
    Routine->readPhyInput();

    // Get temperature
    Routine->readTemp();

    // If wifi & MQTT is connected (then we have time) check sun
    if (isConnected) {
        Routine->checkSun();
    }

    // Check if any delay states are ready to be reset
    Routine->checkDelayed();

    // Write physical outputs
    Routine->writeOutput();

    // If we have coms sent current status
    if (isConnected) {
            Routine->sendStatus(client);
    }

    delay(1000);
}