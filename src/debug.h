//
// Created by andrew on 10/11/20.
//

#ifndef ESP_MQTT_DEBUG_H
#define ESP_MQTT_DEBUG_H


// debug print function
#define debug_print(...) \
            do { if (DEBUG)  Serial.println(__VA_ARGS__); } while (0)


#endif //ESP_MQTT_DEBUG_H
