//
// Created by andrew on 10/10/20.
//

#include "Event.h"

/**
 * Add to transitions map
 * @param current
 * @param destination
 */
void Event::addTransition( const State& current, State* destination) {
   this->transitions[current] = std::unique_ptr<State>(destination);
}

/**
 * Get the next state
 * @param current
 * @return
 */
State* Event::getTargetState(State* current) {
    State* newState = this->transitions[*current].get();

    if (current->getName() != newState->getName()){
        current->setPinState(true);
        newState->setPinState(false);
    }
    return newState;
}

/**
 * Clean up the transitions map
 */
Event::~Event() {
    for(auto it=this->transitions.begin(); it != this->transitions.end(); it++) {
        this->transitions.erase(it);
    }
}