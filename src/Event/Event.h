//
// Created by andrew on 10/10/20.
//

#ifndef ESP_MQTT_EVENT_H
#define ESP_MQTT_EVENT_H
#include <string>
#include <vector>
#include <iterator>
#include <map>
#include <memory>
#include "../State/State.h"
#include "debug.h"
// Define DEBUG to enable debug print statements
#ifndef DEBUG
#define DEBUG 0
#endif

class Event {
private:
    std::map<State,  std::unique_ptr<State>> transitions;
public:
    Event(): transitions(){};
    ~Event();
    void addTransition( const State& current, State* destination);
    State* getTargetState( State* current);
};


#endif //ESP_MQTT_EVENT_H
