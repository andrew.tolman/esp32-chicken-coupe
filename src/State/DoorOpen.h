//
// Created by andrew on 10/10/20.
//

#ifndef ESP_MQTT_DOOROPEN_H
#define ESP_MQTT_DOOROPEN_H


#include "State.h"
#include <string>

class DoorOpen: public State {
public:
    DoorOpen(std::string stateName);
};

#endif //ESP_MQTT_DOOROPEN_H
