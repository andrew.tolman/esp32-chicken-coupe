//
// Created by andrew on 11/22/20.
//

#ifndef ESP_COOP_DOOROPENING_H
#define ESP_COOP_DOOROPENING_H


#include "ActionState.h"

class DoorOpening: public ActionState {
public:
    DoorOpening(std::string, int pinNumber, int secondPinNumber);
};


#endif //ESP_COOP_DOOROPENING_H
