//
// Created by andrew on 10/10/20.
//

#include "State.h"

State::State(std::string name, doorState stateEnum): name(name), state(stateEnum) {}

std::string State::getName(){
    return this->name;
};

/**
 * Virtual function allowing some derived classes
 * to toggle pins through this base class pointer
 * @param value
 */
void State::setPinState(bool value) {}

doorState State::type() {
    return this->state;
}