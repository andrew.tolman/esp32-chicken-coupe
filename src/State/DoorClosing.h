//
// Created by andrew on 11/22/20.
//

#ifndef ESP_COOP_DOORCLOSING_H
#define ESP_COOP_DOORCLOSING_H


#include "ActionState.h"

class DoorClosing: public ActionState {
public:
    DoorClosing(std::string stateName, int pinNumber, int secondPinNumber);
};


#endif //ESP_COOP_DOORCLOSING_H
