//
// Created by andrew on 10/10/20.
//

#ifndef ESP_MQTT_DOORCLOSED_H
#define ESP_MQTT_DOORCLOSED_H

#include "State.h"


class DoorClosed : public State{
public:
    DoorClosed(std::string stateName);
};


#endif //ESP_MQTT_DOORCLOSED_H
