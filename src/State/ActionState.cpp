//
// Created by andrew on 11/22/20.
//

#include "ActionState.h"

ActionState::ActionState(std::string name, int pinNum, int secondPinNum, doorState stateEnum): State(name, stateEnum), pin(pinNum), secondPin(secondPinNum) {}

/**
 *  Since this is an action state we need to account for both physical pins
 * @param value
 */
void ActionState::setPinState(bool value)  {
    this->pin.setPinState(value);
    // only if we are enabling primary then disable secondary
    if (!value) {
        this->secondPin.setPinState(!value);
    }
}
