//
// Created by andrew on 10/10/20.
//

#ifndef ESP_MQTT_STATE_H
#define ESP_MQTT_STATE_H

#define  DOOR_ONE_OPEN_OUT 18
#define  DOOR_ONE_CLOSE_OUT 19
#define  DOOR_TWO_OPEN_OUT 21
#define  DOOR_TWO_CLOSE_OUT 22
#define  STATUS_LED_OUT 23
#define  ERROR_LED_OUT 25
#define  AUX_OPEN_OUT 26
#define  DOOR_ONE_OPEN_IN 16
#define  DOOR_ONE_CLOSE_IN 5
#define  DOOR_TWO_OPEN_IN 4
#define  DOOR_TWO_CLOSE_IN 2

#define COOP_TEMP_SENSOR 32

#include <string>
#include <Input-Output/PinOutput.h>

enum doorState {Open, Opening, Closed, Closing, Nill};

class State {
protected:
    std::string name;
    doorState state;
public:
    State(std::string, doorState);
    virtual ~State()= default;
    doorState type();
    virtual void setPinState(bool);
    std::string getName();
    // overloaded operator for the map order comparison
    bool operator <(const State& rhs) const
    {
        return name < rhs.name;
    }
};


#endif //ESP_MQTT_STATE_H
