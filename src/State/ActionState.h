//
// Created by andrew on 11/22/20.
//

#ifndef ESP_COOP_ACTIONSTATE_H
#define ESP_COOP_ACTIONSTATE_H


#include "State.h"
#include "debug.h"

// Define DEBUG to enable debug print statements
#ifndef DEBUG
#define DEBUG 0
#endif

class ActionState: public State {
private:
    PinOutput pin;
    PinOutput secondPin;
public:
    ActionState(std::string, int pinNum, int secondPinNum, doorState);
    void setPinState(bool);
};


#endif //ESP_COOP_ACTIONSTATE_H
