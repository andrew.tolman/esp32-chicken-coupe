//
// Created by andrew on 10/10/20.
//

#ifndef ESP_MQTT_SCAN_H
#define ESP_MQTT_SCAN_H


#include <Input-Output/PinInput.h>
#include "Context.h"
#include "debug.h"
#include "SunTrack.h"
#include <EspMQTTClient.h>
#include <sys/time.h>
#include "time.h"
#include <Input-Output/TempInput.h>

//
#define LONGITUDE  -119.15550
#define LATITUDE  46.20217
#define TZ_MINUTES -420
#define MILLIS_TO_S 1000
#define ACTUATION_S 90

// Define DEBUG to enable debug print statements
#ifndef DEBUG
#define DEBUG 0
#endif

class Scan {
private:
    Event* DoorOneEvent;
    Event* DoorTwoEvent;
    PinOutput statusLed;
    PinOutput errorLed;
    PinInput DoorOneOpenSwtch;
    PinInput DoorOneCloseSwtch;
    PinInput DoorTwoOpenSwtch;
    PinInput DoorTwoCloseSwtch;
    Context FSM;
    SunTrack TimeTracker;
    std::string error;
    doorState doorOneRemoteOverride;
    doorState doorOneLocalOverride;
    doorState doorTwoRemoteOverride;
    doorState doorTwoLocalOverride;
    TempInput sensorOne;
    const char* ntpServer = "pool.ntp.org";
    const long  gmtOffset_sec = -28800;
    const int   daylightOffset_sec = 3600;
    unsigned long doorOneMotionTargetMillis;
    unsigned long doorTwoMotionTargetMillis;
    float sensorOneTemp;
    void doorOneOutputReset();
    void doorTwoOutputReset();

public:
    Scan();
    void readRemoteInput(const char* method, bool stateCommand);
    void readPhyInput();
    void readTemp();
    void checkSun();
    void writeOutput();
    void checkDelayed();
    void sendStatus(EspMQTTClient& client);
    void setError(std::string& error);
    void setTime();
    std::string getOverrideStatusStrings(doorState state);
};


#endif //ESP_MQTT_SCAN_H
