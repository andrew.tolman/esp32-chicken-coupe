//
// Created by andrew on 10/12/20.
//

#ifndef ESP_MQTT_SUNTRACK_H
#define ESP_MQTT_SUNTRACK_H
#include <Arduino.h>
#include <sys/time.h>
#include "../lib/TimeLord/src/TimeLord.h"
// Define DEBUG to enable debug print statements
#ifndef DEBUG
#define DEBUG 0
#endif

#define TIME_ARRAY_SECOND 0
#define TIME_ARRAY_MINUTE 1
#define TIME_ARRAY_HOUR 2
#define TIME_ARRAY_DAY 3
#define TIME_ARRAY_MONTH 4
#define TIME_ARRAY_YEAR  5
#define DOOR_SUNSET_OFFSET_SECONDS 3600

typedef unsigned char todayArray[6];


class SunTrack {
private:
    TimeLord timeLord;
    long currentSeconds;
    long secondsToSunRise;
    long secondsToSunSet;
    byte today;
    todayArray sunTime;
    struct tm timeStruc;
    char timeBuffer [80];
public:
    SunTrack(int tzminutes, float latitude, float longitude);
    doorState checkTime();
    bool isSameDay();
    void getSunriseSunset();
    static void getSecondsOfDayBytes(todayArray&, long&);
    void setSecondsOfDayTm();
    void getCurrentTimeStr(std::string&);
    void getSunriseStr(std::string & time);
    void getSunsetStr(std::string & time);
};


#endif //ESP_MQTT_SUNTRACK_H
