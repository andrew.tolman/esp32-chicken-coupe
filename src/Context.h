//
// Created by andrew on 10/10/20.
//

#ifndef ESP_MQTT_CONTEXT_H
#define ESP_MQTT_CONTEXT_H


#include <Event/CloseEvent.h>
#include <Event/OpenEvent.h>
#include <Event/OpenedEvent.h>
#include <Event/ClosedEvent.h>
#include <State/DoorOpen.h>
#include <State/DoorClosed.h>
#include <State/DoorOpening.h>
#include <State/DoorClosing.h>
#include "State/State.h"
#include "Event/Event.h"
#include "debug.h"

#ifndef DEBUG
#define DEBUG 0
#endif

class Context {
private:
    State* current_door_one_state;
    State* current_door_two_state;
    DoorOpen doorOneOpenState;
    DoorOpening doorOneOpeningState;
    DoorOpen doorTwoOpenState;
    DoorOpening doorTwoOpeningState;
    DoorClosed doorOneCloseState;
    DoorClosing doorOneClosingState;
    DoorClosed doorTwoCloseState;
    DoorClosing doorTwoClosingState;
public:
    CloseEvent doorOneCloseEvent;
    ClosedEvent doorOneClosedEvent;
    CloseEvent doorTwoCloseEvent;
    ClosedEvent doorTwoClosedEvent;
    OpenEvent doorOneOpenEvent;
    OpenedEvent doorOneOpenedEvent;
    OpenEvent doorTwoOpenEvent;
    OpenedEvent doorTwoOpenedEvent;
    Context();
    void doorOneDispatch(Event*);
    void doorTwoDispatch(Event*);
    void setTransitions();
    std::string getDoorOneStateStr();
    std::string getDoorTwoStateStr();
    doorState getDoorOneState();
    doorState getDoorTwoState();
};


#endif //ESP_MQTT_CONTEXT_H
