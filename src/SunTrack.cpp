//
// Created by andrew on 10/12/20.
//

#include <State/State.h>
#include "SunTrack.h"

/**
 * Setup timelord tz and lat/long
 * @param tzminutes
 * @param latitude
 * @param longitude
 */
SunTrack::SunTrack(int tzminutes, float latitude, float longitude){
    this->timeLord.TimeZone(tzminutes);
    this->timeLord.Position(latitude, longitude);
    this->timeLord.DstRules(3, 2, 11, 1, 60); // second sunday in march thru first sunday in november USA
}

// TODO: Do the computing and return boolean door value
/**
 * 1. get time
 * 2. check if today [day] == this->today [day]
 *    2.a if not then get sunrize & sunset time & set today
 * 3.  If now < sunrize || now > sunset
 *        return false
 *  3.a elseif now < sunset
 *       return true
 *   3.b else panic!!! were bad at maths
 *
 * @return
 */
// get sunrise and sunset time
doorState SunTrack::checkTime() {

    // grab local time
    if (!getLocalTime(&this->timeStruc)){
        Serial.println("Failed to obtain time");
        return Open;
    }

    // 1 check if this->today is the same as the passed  day
    if (!this->isSameDay()){
        debug_print("Check Time: Renew Sunrise Calculation");
        this->getSunriseSunset();
    }

    this->setSecondsOfDayTm();

    // 2. if sunrize > now  > sunset close that door!
    if (this->secondsToSunRise > this->currentSeconds
    || this->currentSeconds > this->secondsToSunSet) {
        return Closed;
        // Check seconds to sunsets + offset
    } else if(this->currentSeconds < this->secondsToSunSet) {
        return Open;
    } else {
        Serial.print(F("Exception trying to calculate time!"));
    }
    return Open;
}

/**
 * Check if time is today
 * @param lft
 * @param rght
 * @return
 */
bool SunTrack::isSameDay() {
    if (this->timeStruc.tm_mday == this->today){
        return true;
    }
    return false;
}

/**
 * Recompute sunrise and set times for today
 * @param current
 */
void SunTrack::getSunriseSunset(){
    // set the byt array for passing to timelord
    this->sunTime[TIME_ARRAY_SECOND] = this->timeStruc.tm_sec;
    debug_print("Seconds: (0-59)");
    debug_print(this->timeStruc.tm_sec);
    // Minutes
    this->sunTime[TIME_ARRAY_MINUTE] = this->timeStruc.tm_min;
    debug_print("Minutes: (0-59)");
    debug_print(this->timeStruc.tm_min);
    // Hours
    this->sunTime[TIME_ARRAY_HOUR] = this->timeStruc.tm_hour;
    debug_print("Hour: (0-23)");
    debug_print(this->timeStruc.tm_hour);
    // Day of month
    this->sunTime[TIME_ARRAY_DAY] = this->timeStruc.tm_mday;
    debug_print("Day: (1-31");
    debug_print(this->timeStruc.tm_mday);
    // Month of year. *tm is 0-11, time loard is 1-12
    this->sunTime[TIME_ARRAY_MONTH] = (this->timeStruc.tm_mon+1);
    debug_print("Month: (1-12)");
    debug_print(this->sunTime[TIME_ARRAY_MONTH]);
    // Year of the Century. *tm is year after 1900, time lord is year after 2000
    this->sunTime[TIME_ARRAY_YEAR] = (this->timeStruc.tm_year-100);
    debug_print("Year: (0-99)");
    debug_print(this->sunTime[TIME_ARRAY_YEAR]);
    this->today = this->timeStruc.tm_mday;

    // Get sunrise time
    this->timeLord.SunRise(this->sunTime);
    SunTrack::getSecondsOfDayBytes(this->sunTime, this->secondsToSunRise);
    debug_print("GetSunriseSeconds: sunrise seconds");
    debug_print(this->secondsToSunRise);

    // get sunset time
    this->timeLord.SunSet(this->sunTime);
    SunTrack::getSecondsOfDayBytes(this->sunTime, this->secondsToSunSet);
    this->secondsToSunSet += DOOR_SUNSET_OFFSET_SECONDS;
    debug_print("GetSunriseSeconds: sunset seconds + offset");
    debug_print(this->secondsToSunSet);
};

/**
 * Convert the byte array to seconds from midnight
 * @param bytes
 * @param seconds
 */
void SunTrack::getSecondsOfDayBytes(todayArray& bytes, long& seconds) {
    seconds =  bytes[TIME_ARRAY_HOUR] * 3600;
    seconds += bytes[TIME_ARRAY_MINUTE] * 60;
    seconds += bytes[TIME_ARRAY_SECOND];
}

/**
 * grab the current second
 */
void SunTrack::setSecondsOfDayTm(){
    this->currentSeconds = this->timeStruc.tm_hour * 3600;
    this->currentSeconds += this->timeStruc.tm_min *60;
    this->currentSeconds += this->timeStruc.tm_sec;
}

/**
 * Grab the current time (current up to the last call of checktim()
 * @param time
 */
void SunTrack::getCurrentTimeStr(std::string &time) {
    // grab local time
    if (!getLocalTime(&this->timeStruc)){
        Serial.println("Failed to obtain time");
        return;
    }
    strftime(timeBuffer, 80, "%I:%M%p %Y", &this->timeStruc);
    time.append(timeBuffer);
}

/**
 *  Return the seconds to sunrise (from midnight)
 * @param time
 */
void SunTrack::getSunriseStr(std::string & time) {
    snprintf(timeBuffer, 80, "%ld", this->secondsToSunRise);
    time.append(timeBuffer);
}

/**
 *  Return the seconds to sunset (from midnight)
 * @param time
 */
void SunTrack::getSunsetStr(std::string & time) {
    snprintf(timeBuffer, 80, "%ld", this->secondsToSunSet);
    time.append(timeBuffer);
}